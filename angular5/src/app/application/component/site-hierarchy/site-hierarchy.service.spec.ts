import { TestBed, inject } from '@angular/core/testing';

import { SiteHierarchyService } from './site-hierarchy.service';

describe('SiteHierarchyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteHierarchyService]
    });
  });

  it('should be created', inject([SiteHierarchyService], (service: SiteHierarchyService) => {
    expect(service).toBeTruthy();
  }));
});
