import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildrenHierarchyComponent } from './children-hierarchy.component';

describe('ChildrenHierarchyComponent', () => {
  let component: ChildrenHierarchyComponent;
  let fixture: ComponentFixture<ChildrenHierarchyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildrenHierarchyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildrenHierarchyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
