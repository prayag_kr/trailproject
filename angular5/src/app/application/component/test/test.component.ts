import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'drop-down-container',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  @Input('multi-select') isMultiSelect: Boolean = false;
  @Input('data') datas: Array<any> = new Array<any>();
  @Output('selected') selectedIds: EventEmitter<Array<String>> = new EventEmitter<Array<String>>();
  private selectedIdList: Array<String> = new Array<String>();
  // private isAllSelected: Boolean = false;
  constructor() { }

  ngOnInit() {

  }

  /**
   * This method will verify if the id has been selected and stored in selectedIdList.
   * @param {String} id required id for which we need to check, if it is selected or not.
   * @returns {Boolean} 
   */
  private isIdSelected(id: String): Boolean {
    return (this.selectedIdList && (this.selectedIdList.indexOf(id) !== -1));
  }

  /**
   * When there is change in the list selection, then this method is called.
   * @param event checked event is triggred when selected or deselect is done.
   * @param {String} id selected id of the list is required, in both case of selected or deselected.
   */
  private checkboxChanged(event, id: String) {
    if (event.target.checked) {
      !this.isMultiSelect ? this.deselectAll() : null
      this.selectedIdList.push(id);
    } else {
      this.selectedIdList.splice(this.selectedIdList.indexOf(id), 1);
    }
    this.emitSelectedIds();
  }

  /**
   * This method is used when we enable Multi Select. It will select all the ids in the list.
   * @param {Boolean} selectAll if it is true then all the list item will be selected, else deselected. also emit data out.
   */
  private selectAll(selectAll: Boolean) {
    if (selectAll) {
      this.deselectAll();
      this.datas.forEach(val => {
        this.selectedIdList.push(val.id);
      });
    } else {
      this.deselectAll();
    }
    this.emitSelectedIds();
  }

  /**
   * This method will check if all the items are selected or not, and will automatically will check the 
   * Select All checkbox to true or false.
   */
  private isAllSelected(): Boolean {
    return (this.datas.length === this.selectedIdList.length);
  }

  /**
   * This method is called to deselect all the item from the list by removing ids from the selectedIdList
   */
  private deselectAll() {
    this.selectedIdList = new Array<String>();
  }

  /**
   * This method is called to emit the list of selected ids to the parent component.
   */
  private emitSelectedIds(){
    this.selectedIds.emit(this.selectedIdList);
  }
}


export class OptionList {
  constructor(public id: String, public value: String) { }
}
