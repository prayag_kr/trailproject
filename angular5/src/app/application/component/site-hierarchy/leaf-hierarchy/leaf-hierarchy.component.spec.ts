import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeafHierarchyComponent } from './leaf-hierarchy.component';

describe('LeafHierarchyComponent', () => {
  let component: LeafHierarchyComponent;
  let fixture: ComponentFixture<LeafHierarchyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeafHierarchyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeafHierarchyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
