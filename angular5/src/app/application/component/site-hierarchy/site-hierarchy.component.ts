import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ParentSite, ChildrenSite, LeafSite } from '../../model/site.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'generic-hierarchy',
  templateUrl: './site-hierarchy.component.html',
  styleUrls: ['./site-hierarchy.component.scss']
})
export class SiteHierarchyComponent implements OnInit, OnChanges {
  @Input('data') data: Array<ParentSite> = new Array<ParentSite>();
  // tslint:disable-next-line:no-input-rename
  @Input('title') titleName: String;
  private filteredData: Array<ParentSite>;
  private selectedIds: Array<String> = new Array<String>('168', '170', '178', '181');
  private memorySelectedIds: Array<String>;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data']) {
      if (this.data) {
        console.log('data came in hierarchy', this.data);
        this.filteredData = this.data;
      }
    }
  }

  private isIdEmiting(ids: Array<String>) {
    console.log('emitted data hierarchy : ' + JSON.stringify(ids));
    this.selectedIds = ids;
    this.memorySelectedIds = ids;
  }

  private removeAll(event) {
    if (event) {
      this.selectedIds = new Array<String>();
    } else {
      this.selectedIds = this.memorySelectedIds;
    }
  }

}
