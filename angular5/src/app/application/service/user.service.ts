import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operator/map'

import { User, UserDetails } from '../model/user';

@Injectable()
export class UserService {

  private httpOptions: any = {};
  private userUrl: string = 'http://localhost:8080/Recharge_Deno/api/app/RECHARGE_DENO/circle/KOL/user';
  private users: Array<User> = new Array<User>();

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };
  }

  public addUser(user: User): void {
    this.users.push(user);
  }

  public getUsers(): Array<User> {
    return this.users;
  }


  public getUserDetails(): Observable<any> {
    return this.http.get<any>(this.userUrl, this.httpOptions);

  }

}
