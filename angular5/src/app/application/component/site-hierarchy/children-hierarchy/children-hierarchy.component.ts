import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { ParentSite, ChildrenSite, LeafSite } from '../../../model/site.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'children-hierarchy',
  templateUrl: './children-hierarchy.component.html',
  styleUrls: ['./children-hierarchy.component.scss']
})
export class ChildrenHierarchyComponent implements OnInit, OnChanges {
  // @Input('data') countries: Array<ChildrenSite | ParentSite> = new Array<ChildrenSite | ParentSite>();
  // tslint:disable-next-line:no-input-rename
  @Input('data') countries: Array<any> = new Array<any>();
  // tslint:disable-next-line:no-input-rename
  @Input('selectedIds') selectedIds: Array<String> = new Array<String>();
  @Output('sendSelectedIds') sendSelectedIds: EventEmitter<Array<String>> = new EventEmitter<Array<String>>();
  private thisChildLeaf: Array<Array<String>>;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('child triggered');
    if (changes['selectedId']) {
      if (this.selectedIds) {
        console.log('data in child ', JSON.stringify(this.selectedIds));
        this.selectedIds = this.selectedIds;
      }
    }

    if (changes['countries']) {
      if (this.countries) {
        // This for level at which Parent of Child Node is available. It can be verified by property children. Ref : ParentSite Object.
        if (this.countries[0].children) {
          console.log('Parent');
        }
        // This for level at which Parent of Leaf Node is available. It can be verified by property sites. Ref : ChildrenSite Object.
        if (this.countries[0].sites) {
          // thisChildLeaf[i] is like using a city  (Object used here is ChildrenSite) 
          // thisChildLeaf[i].sites[j] is like using sites in a city. (Object used here is LeafSite)
          this.thisChildLeaf = new Array<Array<String>>();    // [ [''168', '170'], ['171'], [] ]
          for (let i = 0; i < this.countries.length; i++) {
            this.thisChildLeaf[i] = new Array<String>();
            for (let j = 0; j < this.countries[i].sites.length; j++) {
              this.thisChildLeaf[i].push(this.countries[i].sites[j].site_id);
            }
          }
        }
      }
    }

  }

  /**
   * This method will check if all the Leaf nodes are selected or not.
   * If selected then Parent node of Leaf of that index will be selected.
   * @param {number} index index array for which check is requried.
   */
  private isAllLeafChecked(index: number): Boolean {
    let allLeafChecked: Boolean = true;
    // console.log('thischildren leaf : ' + index + ' : ' + this.countries[index].name);
    // console.log('thischildren leaf : ' + index + ' : ' + JSON.stringify(this.thisChildLeaf[index]));
    if (this.thisChildLeaf[index].length > 0) {
      this.thisChildLeaf[index].forEach((ids: String) => {
        (allLeafChecked) ? ((this.isIdSelected(ids)) ? allLeafChecked = true : allLeafChecked = false) : allLeafChecked = false;
      });
    } else {
      allLeafChecked = false;
    }
    return allLeafChecked;
  }

  /**
   * When ever there is a change in the Leaf Parent, of selected index. this method is executed.
   * If the Leaf Parent is checked, it will select all the sites under it, else remove all the sites.
   * @param {Boolean} isChecked Leaf Parent is checked or not
   * @param {number} index Leaf Parent index.
   */
  private selectLeafChange(isChecked: Boolean, index: number) {
    this.thisChildLeaf[index].forEach((ids: String) => {
      (this.isIdSelected(ids)) ? this.removedSelectedId(ids) : null;
      (isChecked) ? this.selectedIds.push(ids) : null;
    });
    this.isIdEmiting(this.selectedIds);
  }

  /**
   * remove the id from the list of selectedIds. It is used to unSelect all leaf sites
   * @param {String} id site_id for which id is removed
   */
  private removedSelectedId(id: String) {
    this.selectedIds.splice(this.selectedIds.indexOf(id), 1);
  }

  /**
   * It will check, if the selectedIds array is defined and it contains the id provided
   * @param {String} id site_id for which check is requred
   */
  private isIdSelected(id: String): Boolean {
    return (this.selectedIds && this.selectedIds.indexOf(id) !== -1);
  }

  private isIdEmiting(ids: Array<String>) {
    console.log('emitted data child : ' + JSON.stringify(ids));
    this.sendSelectedIds.emit(ids);
    // this.selectedIds = [];
  }

}
