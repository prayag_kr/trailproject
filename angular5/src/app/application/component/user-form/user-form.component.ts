import { Component, OnInit } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  private userForm: FormGroup;
  private user: User = new User();

  constructor(
    private _fb: FormBuilder, 
    private router: Router, 
    private userService : UserService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.userForm =  this._fb.group({
        name: this._fb.control(''),
        age: this._fb.control(''),
        gender: this._fb.control('')
      })
    
  }

  private submitData() {
    console.log('data : ', this.userForm.value);

    this.userService.addUser(this.userForm.value);
    this.backToView();
  }

  private backToView() {
    this.router.navigateByUrl('user/view');
  }

}
