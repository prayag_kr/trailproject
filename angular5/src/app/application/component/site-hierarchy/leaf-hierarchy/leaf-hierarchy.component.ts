import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { ParentSite, ChildrenSite, LeafSite } from '../../../model/site.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'leaf-hierarchy',
  templateUrl: './leaf-hierarchy.component.html',
  styleUrls: ['./leaf-hierarchy.component.scss']
})
export class LeafHierarchyComponent implements OnInit, OnChanges {
  @Input('sites') sites: Array<LeafSite> = new Array<LeafSite>();
  @Input('selectedIds') selectedIds: Array<String> = new Array<String>();
  @Input() allLeafChecked: Boolean;
  @Output('sendSelectedIds') sendSelectedIds: EventEmitter<Array<String>> = new EventEmitter<Array<String>>();

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('leaf triggered');
    if (changes['selectedIds']) {
      if (this.selectedIds) {
        console.log('data in leaf ', JSON.stringify(this.selectedIds));
        // this.filteredData = this.sites;
      }
    }
    if (changes['allLeafChecked']) {
      if (this.allLeafChecked) {
        console.log('all : ' + this.allLeafChecked);
      }
    }
  }

  private isSiteSelected(siteId: String): Boolean {
    return (this.selectedIds && this.selectedIds.indexOf(siteId) !== -1) ? true : false;
  }

  private siteAction(isChecked: Boolean, siteId: String) {
    // tslint:disable-next-line:no-unused-expression
    (isChecked) ? ( (!this.isSiteSelected(siteId)) ? this.selectedIds.push(siteId) : null  ) :
                  this.selectedIds.splice(this.selectedIds.indexOf(siteId), 1);
    this.sendSelectedIds.emit(this.selectedIds);
  }

}
