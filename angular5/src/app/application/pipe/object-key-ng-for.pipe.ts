import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKeyNgFor',
  pure: false
})
export class ObjectKeyNgForPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Object.keys(value).map(key => {
      return Object.assign({ key }, value[key] );
    });
  }

}
