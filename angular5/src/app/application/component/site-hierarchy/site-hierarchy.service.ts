import { Injectable } from '@angular/core';
import { ParentSite, ChildrenSite, LeafSite } from '../../model/site.model';

@Injectable()
export class SiteHierarchyService {
  private leaf1: LeafSite;
  private leaf2: LeafSite;
  private leaf3: LeafSite;
  private child1: ChildrenSite;
  private child2: ChildrenSite;
  private child3: ChildrenSite;
  private kat1: LeafSite;
  private kat2: LeafSite;
  private kat3: LeafSite;
  private kakar: ChildrenSite;
  private khatmandu: ChildrenSite;
  private pokhra: ChildrenSite;
  private india: ParentSite;
  private nepal: ParentSite;
  private bhutan: ParentSite;
  private andromeda1: ParentSite;
  constructor() {
    this.initSites();
  }

  private initSites() {
    this.leaf1 = new LeafSite('698', 'Electronic City', '691', '685, 686, 691, 698', false, 3, 'site', true, '168');
    this.leaf2 = new LeafSite('702', 'White Field', '691', '685, 686, 691, 702', false, 3, 'site', true, '170');
    this.leaf3 = new LeafSite('703', 'Sarjapur', '691', '685, 686, 691, 703', false, 3, 'site', true, '171');
    this.child1 = new ChildrenSite('691', 'Banglore', '686', '685, 686, 691', false, 2, 'City', true, new Array<LeafSite>(this.leaf1, this.leaf2, this.leaf3));
    this.child2 = new ChildrenSite('692', 'Delhi', '686', '685, 686, 692', false, 2, 'City', true, new Array<LeafSite>());
    this.child3 = new ChildrenSite('693', 'Kolkata', '686', '685, 686, 693', false, 2, 'City', true, new Array<LeafSite>());
    this.india = new ParentSite('686', 'India', '685', '685, 686', false, 1, 'Country', true, new Array<ChildrenSite>(this.child1, this.child2, this.child3));

    this.kat1 = new LeafSite('798', 'Kat City', '792', '685, 687, 791, 798', false, 3, 'site', true, '178');
    this.kat2 = new LeafSite('799', 'Kat Field', '792', '685, 687, 791, 799', false, 3, 'site', true, '180');
    this.kat3 = new LeafSite('800', 'Kat Pur', '793', '685, 687, 791, 800', false, 3, 'site', true, '181');
    this.kakar = new ChildrenSite('791', 'kakar', '687', '685, 687, 791', false, 2, 'City', true, new Array<LeafSite>());
    this.khatmandu = new ChildrenSite('792', 'khatmandu', '687', '685, 687, 792', false, 2, 'City', true, new Array<LeafSite>(this.kat1, this.kat2));
    this.pokhra = new ChildrenSite('793', 'pokhra', '687', '685, 687, 793', false, 2, 'City', true, new Array<LeafSite>(this.kat3));
    this.nepal = new ParentSite('687', 'Nepal', '685', '685, 687', false, 1, 'Country', true, new Array<ChildrenSite>(this.kakar, this.khatmandu, this.pokhra));

    this.bhutan = new ParentSite('688', 'Bhutan', '685', '685, 688', false, 1, 'Country', true,
                                                                            new Array<ChildrenSite>(this.child1, this.child2, this.child3));

    this.andromeda1 = new ParentSite('685', 'Andromeda 1', 'null', '685', false, 1, 'Company', true, new Array<ParentSite>(this.india, this.nepal));

  }

  public getSites(): Array<ParentSite> {
    // tslint:disable-next-line:prefer-const
    let hierarchy: Array<ParentSite> = new Array<ParentSite>();
    hierarchy.push(this.andromeda1);

    return hierarchy;
  }
}
