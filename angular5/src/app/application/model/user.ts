
export class User {    
    constructor(public name ?: String, public age ?: Number, public gender ?: String ) { }
}

export class UserDetails {
    constructor(
        public appNameProcess?: String,
        public appType?: String,
        public attempts?: String,
        public circle?: String,
        public creationDate?: Date,
        public defaultPassword?: String,
        public hodName?: String,
        public lastLoggedOn?: Date,
        public loginIp?: String,
        public marketCode?: String,
        public nameInitial?: String,
        public oldPassword1?: String,
        public oldPassword2?: String,
        public oldPassword3?: String,
        public oldPassword4?: String,
        public passExpDate?: String,
        public passStatus?: String,
        public password?: String,
        public userAuthTypeId?: String,
        public userEmail?: String,
        public userId?: String,
        public userMobile?: String,
        public userName?: String,
    ) { }
}