import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { UserFormComponent } from './application/component/user-form/user-form.component';
import { UserViewComponent } from './application/component/user-view/user-view.component';
import { UserService } from './application/service/user.service';
import { TestComponent } from './application/component/test/test.component';
import { DropdownDirective } from './application/directive/dropdown.directive';
import { DynamicTableComponent } from './application/component/dynamic-table/dynamic-table.component';
import { ObjectKeyNgForPipe } from './application/pipe/object-key-ng-for.pipe';
import {
          SiteHierarchyComponent, ChildrenHierarchyComponent, LeafHierarchyComponent, SiteHierarchyService
        } from './application/component/site-hierarchy';

const projectRoute: Routes = [
  {
    path: '',
    redirectTo: 'user/view',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: AppComponent,
    children: [
      {
        path: 'add',
        component: UserFormComponent
      },
      {
        path: 'view',
        component : UserViewComponent
      },
      {
        path: 'test',
        component: TestComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    UserViewComponent,
    TestComponent,
    DropdownDirective,
    DynamicTableComponent,
    ObjectKeyNgForPipe,
    SiteHierarchyComponent,
    ChildrenHierarchyComponent,
    LeafHierarchyComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(projectRoute, { useHash: true })
  ],
  providers: [UserService, SiteHierarchyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
