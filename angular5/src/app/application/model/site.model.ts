export class ParentSite {
    constructor(
        public id: String,
        public name: String,
        public parent_id: String,
        public breadcrumbs: String,
        public is_deleted: Boolean,
        public path_length: Number,
        public type: String,
        public active: Boolean,
        public children: Array<ChildrenSite | ParentSite>
    ) {}
}

export class ChildrenSite {
    constructor(
        public id: String,
        public name: String,
        public parent_id: String,
        public breadcrumbs: String,
        public is_deleted: Boolean,
        public path_length: Number,
        public type: String,
        public active: Boolean,
        public sites: Array<LeafSite>
    ) {}
}

export class LeafSite {
    constructor(
        public id: String,
        public name: String,
        public parent_id: String,
        public breadcrumbs: String,
        public is_deleted: Boolean,
        public path_length: Number,
        public type: String,
        public active: Boolean,
        public site_id: String
    ) {}
}
