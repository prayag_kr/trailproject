import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, UserDetails } from '../../model/user';
import { UserService } from '../../service/user.service';
import { OptionList } from '../test/test.component';
import { ParentSite, ChildrenSite, LeafSite } from '../../model/site.model';
import { SiteHierarchyService } from '../site-hierarchy';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {
  private users: Array<User> = new Array<User>();
  private datas: Array<OptionList> = new Array<OptionList>();
  private userDetails: Array<UserDetails> = new Array<UserDetails>();
  private tableHead: Array<String> = new Array<String>();
  private tableColName: Array<String> = new Array<String>();
  private filteredData: Array<ParentSite>;

  constructor(
    private router: Router,
    private userService: UserService,
    private siteHierarchyService: SiteHierarchyService
  ) { }

  ngOnInit() {
    this.users = this.userService.getUsers();
    this.getUserDetails();
    this.datas.push(new OptionList('123', 'level 1'));
    this.datas.push(new OptionList('456', 'level 2'));
    this.datas.push(new OptionList('789', 'level 3'));
    this.datas.push(new OptionList('246', 'level 4'));
    this.datas.push(new OptionList('358', 'level 5'));
    this.datas.push(new OptionList('469', 'level 6'));
    this.datas.push(new OptionList('579', 'level 7'));
    this.tableHead = ['Process', 'Type', 'Attempts', 'Circle', 'Created', 'Default Password', 'HOD Name', 'Last Logged In',
      'Login Ip', 'Market Code', 'Initial', 'Old Password 1', 'Old Password 2', 'Old Password 3',
      'Old Password 4', 'Password Exp Date', 'Password Status', 'Password', 'Auth Type Id', 'Email', 'User Id',
      'Mobile No', 'User Name'];
    // this.tableColName = ['appNameProcess', 'appType', 'attempts', 'circle', 'creationDate', 'defaultPassword', 'hodName', 'lastLoggedOn',
    //                       'loginIp', 'marketCode', 'nameInitial', 'oldPassword1', 'oldPassword2', 'oldPassword3', 'oldPassword4',
    //                       'passExpDate', 'passStatus', 'password', 'userAuthTypeId', 'userEmail', 'userId', 'userMobile', 'userName'];
    this.getHierarchy();
  }

  private navigateToAddUser() {
    this.router.navigateByUrl('user/add');
  }

  private getUserDetails() {
    // this.userService.getUserDetails().subscribe((res: Array<UserDetails>) => {
    //   if (res) {
    //     console.log('user data', res);
    //     this.userDetails = res;
    //   }
    // });
  }

  private selectedId(selectedId: Array<String>) {
    console.log('selected id ' + JSON.stringify(selectedId));
  }

  private getHierarchy() {
    setTimeout(() => {
      this.filteredData = this.siteHierarchyService.getSites();
      console.log('data', this.filteredData);
    }, 2000);
  }

}
