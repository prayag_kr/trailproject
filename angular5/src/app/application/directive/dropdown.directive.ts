import { Directive, ElementRef, HostListener, HostBinding, Renderer, OnInit } from '@angular/core';

@Directive({
  selector: '[dropDown]'
})
export class DropdownDirective implements OnInit  {
  private isOpen: boolean = false;
  private ul: ElementRef;

  constructor( private elementRef: ElementRef, private renderer: Renderer ) { }

  ngOnInit() {
    this.ul = this.elementRef.nativeElement.querySelector('ul');
    this.renderer.setElementClass(this.ul, 'toggle-drop-down', this.isOpen);
}

  @HostListener('document: click', ['$event'])
  toggle($event: MouseEvent) {
    if (this.elementRef.nativeElement.contains($event.target)) {
      this.isOpen = true;
    } else {
      this.isOpen = false;
    }
    this.renderer.setElementClass(this.ul, 'toggle-drop-down', this.isOpen);
  }

}
